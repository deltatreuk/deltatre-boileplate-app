{
    'managementApi': {
        'authKey': 'c8e5a7eb-268a-4d6e-be6e-2cc725110e3f',
        'url': 'https://gk-rfu-dev-forge.azurewebsites.net/'
    },
    'serviceBus': {
        'url': 'amqp://guest:guest@localhost:5672'
    },
    'distributionApi': {
        'url': 'https://gk-rfu-dev-forge-dapi.azurewebsites.net/'
    },
    'frontEnd': {
        'url': 'http://gk-rfu-dev-fe-en.azurewebsites.net/',
        'authKey': '6f253d4a-09f3-47f7-9026-968a6d4ec97b'
    }
}
