module.exports = grunt => {
    'use strict';
    require('load-grunt-tasks')(grunt);
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        exec: {
            android: 'node node_modules/react-native-scripts/build/bin/react-native-scripts.js android',
            eject: 'node node_modules/react-native-scripts/build/bin/react-native-scripts.js eject',
            ios: 'node node_modules/react-native-scripts/build/bin/react-native-scripts.js ios',
            start: 'node node_modules/react-native-scripts/build/bin/react-native-scripts.js start',
            test: 'node node_modules/jest/bin/jest.js --watch'
        }
    });

    grunt.registerTask('default', ['exec:start']);
};
