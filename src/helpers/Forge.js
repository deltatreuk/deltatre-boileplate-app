//import sdk from 'forge-nodejs-sdk';
const sdk = require('forge-nodejs-sdk');

const ForgeManagementApi = sdk.ForgeManagementApi;
const ForgeNotificationBus = sdk.ForgeNotificationBus;
const DistributionNotificationBus = sdk.DistributionNotificationBus;
const ForgeCommands = sdk.ForgeCommands;
const ForgeDistributionApi = sdk.ForgeDistributionApi;
const ForgeFrontEndApi = sdk.ForgeFrontEndApi;

var config = {
    'managementApi': {
        'authKey': 'c8e5a7eb-268a-4d6e-be6e-2cc725110e3f',
        'url': 'https://gk-rfu-dev-forge.azurewebsites.net/'
    },
    'serviceBus': {
        'url': 'Endpoint=sb://gk-rfu-dev-servicebus.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=WuYEsRSNoIjGjWwNCcbihd6+sBGSo8cltxgj15pkkjc='
    },
    'distributionApi': {
        'url': 'https://gk-rfu-dev-forge-dapi.azurewebsites.net/'
    },
    'frontEnd': {
        'url': 'http://gk-rfu-dev-fe-en.azurewebsites.net/',
        'authKey': '6f253d4a-09f3-47f7-9026-968a6d4ec97b'
    }
};

let api = new ForgeFrontEndApi(config.frontEnd);
api.getApi("/cms/api/templates", {
    format: "json",
    url: ''
}).then((stories) => {
    console.log(stories);
});

export default api;
