import React                        from 'react';
import { Text, ScrollView }  from 'react-native';
import FitImage                     from 'react-native-fit-image';
import removeMd                     from 'remove-markdown';
import PropTypes                    from 'prop-types';

class StoryDetailController extends React.Component {
    static navigationOptions = {
        title: 'Story Detail',
    };

    buildParts (l, i) {
        switch (l.type) {
        case 'markdown':
            return <Text key={i}>{removeMd(l.content)}</Text>;
        case 'photo':
            return <FitImage key={i} source={{ uri: l.image.thumbnailUrl }} style={{ marginTop: 10, marginBottom: 10 }} />;
        default:
            return null;
        }
    }

    render () {
        const { params } = this.props.navigation.state;
        var itemParts = params.item.parts.map((l, i) => {
            return this.buildParts(l,i);
        });

        return (
            <ScrollView style={{ paddingLeft: 20, paddingRight: 20 }}>
                <Text style={{ paddingTop: 20, paddingBottom: 10, fontSize: 30, fontWeight: 'bold' }}>{params.item.title}</Text>
                <Text style={{ paddingBottom: 10, fontSize: 20, fontWeight: 'bold' }}>{params.item.headline}</Text>
                { itemParts }
            </ScrollView>
        );
    }
}

StoryDetailController.propTypes = {
    navigation: PropTypes.object
};

export default StoryDetailController;
