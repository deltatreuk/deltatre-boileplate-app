import React                    from 'react';
import { StyleSheet, Image }    from 'react-native';
import { StackNavigator }       from 'react-navigation';

import StoryListController      from './StoryListController';
import StoryDetailController    from './StoryDetailController';

const styles = StyleSheet.create({
    icon: {
        width: 26,
        height: 26
    }
});

const Stack = StackNavigator({
    List: { screen: StoryListController },
    Detail: { screen: StoryDetailController }
});

class StoryNavigatorController extends React.Component {
    static navigationOptions = {
        tabBarLabel: 'Stories',
        tabBarIcon: ({ tintColor }) => (
            <Image
                source={require('../../../assets/icons/icon-news.png')}
                style={[styles.icon, { tintColor: tintColor }]}
            />
        )
    };

    render () {
        return <Stack />;
    }
}

export default StoryNavigatorController;
