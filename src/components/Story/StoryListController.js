import React                    from 'react';
import { ScrollView }           from 'react-native';
import { List, ListItem }       from 'react-native-elements';
import { connect }              from 'react-redux';
import { bindActionCreators }   from 'redux';
import PropTypes                from 'prop-types';

import * as Actions             from '../../actions';

class StoryListController extends React.Component {
    static navigationOptions = {
        title: 'Stories'
    };

    componentDidMount () {
        this.props.actions.getStories();
    }

    render () {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <List containerStyle={{ marginBottom: 20 }}>
                    {
                        this.props.forgeAPI.stories.map((l, i) => (
                            <ListItem
                                roundAvatar
                                avatar={(l.thumbnail?{ uri: l.thumbnail.thumbnailUrl }:{ uri: 'http://gk-rfu-dev-fe-en.azurewebsites.net/assets/images/logo-rfu.png' })}
                                key={i}
                                title={l.title}
                                onPress={() => navigate('Detail', { item: l })}
                            />
                        ))
                    }
                </List>
            </ScrollView>
        );
    }
}

StoryListController.propTypes = {
    actions: PropTypes.object,
    navigation: PropTypes.object,
    forgeAPI: PropTypes.object
};

const mapStateToProps = state => ({
    forgeAPI: state.forgeAPI
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({ ...Actions }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(StoryListController);
