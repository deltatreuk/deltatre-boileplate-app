import React                            from 'react';
import { TabNavigator }                 from 'react-navigation';
import { Provider }                     from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import promise                          from 'redux-promise-middleware';
//import logger                           from 'redux-logger'

import reducer                          from '../../reducers';
import StoryNavigatorController         from '../Story/StoryNavigatorController';
import PhotoNavigatorController         from '../Photo/PhotoNavigatorController';
import ForgeNavigatorController         from '../Forge/ForgeNavigatorController';

const Tabs = TabNavigator({
    Stories: { screen: StoryNavigatorController },
    Photos:  { screen: PhotoNavigatorController },
    Forge:   { screen: ForgeNavigatorController }
}, {
    tabBarOptions: {
        activeTintColor: '#a61930',
    },
});

const store = createStore(reducer, applyMiddleware(promise()));

export default class AppContainer extends React.Component {
    render () {
        return (
            <Provider store={store}>
                <Tabs />
            </Provider>
        );
    }
}
