import React                    from 'react';
import { StyleSheet, Image }    from 'react-native';
import { StackNavigator }       from 'react-navigation';

import ForgeListController      from './ForgeListController';
import ForgeDetailController    from './ForgeDetailController';

const styles = StyleSheet.create({
    icon: {
        width: 26,
        height: 26
    }
});

const Stack = StackNavigator({
    List: { screen: ForgeListController },
    Detail: { screen: ForgeDetailController }
});

class ForgeNavigatorController extends React.Component {
    static navigationOptions = {
        tabBarLabel: 'Forge',
        tabBarIcon: ({ tintColor }) => (
            <Image
                source={require('../../../assets/icons/icon-settings.png')}
                style={[styles.icon, { tintColor: tintColor }]}
            />
        )
    };

    render () {
        return <Stack />;
    }
}

export default ForgeNavigatorController;
