import React          from 'react';
import { ScrollView } from 'react-native';
import JSONTree       from 'react-native-json-tree';
import PropTypes      from 'prop-types';

import { THEME }      from '../../constants/JsonViewerTheme';

class ForgeDetailController extends React.Component {
    static navigationOptions = {
        title: 'Forge Detail',
    };

    render () {
        const { params } = this.props.navigation.state;
        return (
            <ScrollView style={{ backgroundColor: '#fff', flex: 1, paddingLeft: 0,  paddingRight: 0 }} >
                <JSONTree data={params.item} theme={THEME} />
            </ScrollView>
        );
    }
}

ForgeDetailController.propTypes = {
    navigation: PropTypes.object
};

export default ForgeDetailController;
