import React                    from 'react';
import { ScrollView }           from 'react-native';
import { List, ListItem }       from 'react-native-elements';
import { connect }              from 'react-redux';
import { bindActionCreators }   from 'redux';
import PropTypes                from 'prop-types';

import * as Actions             from '../../actions';

class ForgeListController extends React.Component {
    static navigationOptions = {
        title: 'Forge'
    };

    componentDidMount () {
        this.props.actions.getStories();
    }

    render () {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <List containerStyle={{ marginBottom: 20 }} >
                    <ListItem title={'Get Stories'} leftIcon={{ name: 'subtitles' }} onPress={() => navigate('Detail', { item: this.props.forgeAPI.stories })} />
                    <ListItem title={'Get Photos'} leftIcon={{ name: 'photo-camera' }} onPress={() => navigate('Detail', { item: this.props.forgeAPI.photos })} />
                </List>
            </ScrollView>
        );
    }
}

ForgeListController.propTypes = {
    actions: PropTypes.object,
    navigation: PropTypes.object,
    forgeAPI: PropTypes.object
};

const mapStateToProps = state => ({
    forgeAPI: state.forgeAPI
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({ ...Actions }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(ForgeListController);
