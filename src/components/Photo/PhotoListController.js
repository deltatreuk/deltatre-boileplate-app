import React                    from 'react';
import { connect }              from 'react-redux';
import { bindActionCreators }   from 'redux';
import {
    View,
    Image,
    ScrollView,
    TouchableHighlight }        from 'react-native';
import PropTypes                from 'prop-types';

import * as Actions             from '../../actions';

class PhotoListController extends React.Component {
    static navigationOptions = {
        title: 'Photo'
    };

    componentDidMount () {
        this.props.actions.getPhotos();
    }

    render () {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap' }}>
                    {
                        this.props.forgeAPI.photos.map((l, i) => (
                            <TouchableHighlight key={i} onPress={() => navigate('Detail', { item: l.image.thumbnailUrl })}>
                                <Image source={{ uri: l.image.thumbnailUrl }}
                                    style={{ width: 125, height: 125 }}
                                />
                            </TouchableHighlight>
                        ))
                    }
                </View>
            </ScrollView>
        );
    }
}

PhotoListController.propTypes = {
    actions: PropTypes.object,
    navigation: PropTypes.object,
    forgeAPI: PropTypes.object
};


const mapStateToProps = state => ({
    forgeAPI: state.forgeAPI
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({ ...Actions }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(PhotoListController);
