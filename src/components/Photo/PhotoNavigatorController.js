import React                    from 'react';
import { StyleSheet, Image }    from 'react-native';
import { StackNavigator }       from 'react-navigation';

import PhotoListController      from './PhotoListController';
import PhotoDetailController    from './PhotoDetailController';

const styles = StyleSheet.create({
    icon: {
        width: 26,
        height: 26
    }
});

const Stack = StackNavigator({
    List: { screen: PhotoListController },
    Detail: { screen: PhotoDetailController }
});

export default class PhotoNavigatorController extends React.Component {
    static navigationOptions = {
        tabBarLabel: 'Photos',
        tabBarIcon: ({ tintColor }) => (
            <Image
                source={require('../../../assets/icons/icon-photo.png')}
                style={[styles.icon, { tintColor: tintColor }]}
            />
        )
    };

    render () {
        return <Stack />;
    }
}
