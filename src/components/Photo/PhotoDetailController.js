import React             from 'react';
import ImageViewer       from 'react-native-image-zoom-viewer';
import {
    StyleSheet,
    Image,
    Modal,
    TouchableHighlight } from 'react-native';
import PropTypes         from 'prop-types';

const styles = StyleSheet.create({
    header: {
        backgroundColor: '#333'
    },
    headerTitle: {
        color: '#fff'
    },
    main: {
        flex: 1,
        backgroundColor: '#333'
    },
    closeContainer: {
        position: 'absolute',
        top: 40,
        right: 20,
        zIndex: 2
    },
    closeImage: {
        width: 30,
        height: 30,
        tintColor: 'white'
    }
});

class PhotoDetailController extends React.Component {
    static navigationOptions = {
        title: 'Photo Detail',
        headerTintColor: '#fff',
        headerTitleStyle: styles.headerTitle,
        headerStyle: styles.header
    };

    render () {
        const { params } = this.props.navigation.state;
        const { goBack } = this.props.navigation;
        return (

            <Modal visible={true} transparent={false}>
                <TouchableHighlight style={styles.closeContainer}
                    onPress={() => goBack(null)}>
                    <Image source={require('../../../assets/icons/icon-close.png')}
                        style={styles.closeImage}
                    />
                </TouchableHighlight>
                <ImageViewer imageUrls={[{
                    url: params.item
                }]}
                />
            </Modal>
        );
    }
}

PhotoDetailController.propTypes = {
    navigation: PropTypes.object
};

export default PhotoDetailController;
