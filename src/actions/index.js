import axios      from 'axios';

import { config } from '../config';
import * as types from '../constants/ActionsTypes';

export const getStories = () => ({
    type: types.GET_STORIES,
    payload: {
        promise: axios.get(config.distributionApi.url + '/v1/content/en-GB/stories'),
        data: {}
    }
});

export const getPhotos = () => ({
    type: types.GET_PHOTOS,
    payload: {
        promise: axios.get(config.distributionApi.url + '/v1/content/en-GB/Photos'),
        data: {}
    }
});
