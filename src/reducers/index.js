import { combineReducers } from 'redux';
import forgeAPI from './forgeAPI';

export default combineReducers({
    forgeAPI
});
