import * as types from '../constants/ActionsTypes';

const initialState = {
    stories: [],
    photos: []
};

export default (state = initialState, action) => {
    switch (action.type) {
    case types.GET_STORIES_FULFILLED:
        return Object.assign({}, state, { stories: action.payload.data.items });
    case types.GET_STORIES_PENDING:
        return state;
    case types.GET_STORIES_REJECTED:
        return state;
    case types.GET_PHOTOS_FULFILLED:
        return Object.assign({}, state, { photos: action.payload.data.items });
    case types.GET_PHOTOS_PENDING:
        return state;
    case types.GET_PHOTOS_REJECTED:
        return state;
    // case types.GET_STORIES1:
    //     return state.map(todo =>
    //         (todo.id === action.id)
    //           ? {...todo, completed: !todo.completed}
    //           : todo
    //     )
    default:
        return state;
    }
};
