export const config = {
    deltatreApi: {
        url: 'http://cdn.results.rfu.deltatre.net'
    },
    season: '2017-2018',
    distributionApi: {
        url: 'http://gk-rfu-dev-forge-dapi.azurewebsites.net'
    }
};
