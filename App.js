import { AppRegistry } from 'react-native';
import App from './src/components/Main/AppContainer';
export default App;
if(typeof global.self === 'undefined') { global.self = global; }
AppRegistry.registerComponent('DELTATRE-BOILERPLATE-APP', () => App);
